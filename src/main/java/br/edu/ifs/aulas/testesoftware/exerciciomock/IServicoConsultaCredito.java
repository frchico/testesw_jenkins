package br.edu.ifs.aulas.testesoftware.exerciciomock;

import br.edu.ifs.aulas.testesoftware.exerciciomock.exceptions.NetworkError;
import br.edu.ifs.aulas.testesoftware.exerciciomock.exceptions.ParametroInvalidoException;

import java.util.List;

/**
 * Created by Francisco on 23/11/2016.
 */
public interface IServicoConsultaCredito {
    /**
     * Realiza a consulta do CPF para saber se está inserido em algum cadastro restritivo
     * @param CPF CPF a ser consultado sem a máscara
     * @return
     * @throws ParametroInvalidoException
     * @throws NetworkError
     */
    boolean estahNoSPC(String CPF) throws ParametroInvalidoException, NetworkError;

    /**
     * Obtem uma lista de pendências do cliente
     * @param cpf
     * @return
     * @throws ParametroInvalidoException
     * @throws NetworkError
     */
    List<IPendencia> obterPendencias(String cpf) throws ParametroInvalidoException, NetworkError;

    /**
     * Retorna o limite para financiamento do cliente.
     * Clientes com restrição cadastral não possuem limite
     * @param cpf
     * @return
     * @throws ParametroInvalidoException
     * @throws NetworkError
     */
    double obterLimiteCredito(String cpf)  throws ParametroInvalidoException, NetworkError;
}

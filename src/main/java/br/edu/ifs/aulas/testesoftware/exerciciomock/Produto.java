package br.edu.ifs.aulas.testesoftware.exerciciomock;

import java.util.Date;
import java.util.List;

/**
 * Created by Francisco on 23/11/2016.
 */
public class Produto {
    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;

    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    private String codigo;
    private double peso;
    private double valor;

}

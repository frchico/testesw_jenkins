package br.edu.ifs.aulas.testesoftware.exerciciomock;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Francisco on 23/11/2016.
 */
public class ProdutoDAO {

    static ProdutoDAO produtoDAO;
    public static ProdutoDAO getProdutoDAO(){
        if (produtoDAO == null)
            produtoDAO = new ProdutoDAO();
        return produtoDAO;
    }
    final List<Produto> produtoList = new ArrayList<Produto>();
    private ProdutoDAO() {
        Random r = new Random();
        for (int i = 0; i < 10; i++) {
            Produto p = new Produto();
            p.setCodigo(RandomStringUtils.randomNumeric(6));
            p.setPeso(r.nextDouble());
            p.setValor(r.nextDouble());
            produtoList.add(p);
        }
    }
    public Produto obterProduto(String codigo){
        for( Produto p : produtoList)
            if ( p.getCodigo().equals(codigo))
                return p;
        return null;
    }
    public List<Produto> obterProduto(){
        return produtoList;
    }




}

package br.edu.ifs.aulas.testesoftware.exerciciomock;

import br.edu.ifs.aulas.testesoftware.exerciciomock.exceptions.NetworkError;
import br.edu.ifs.aulas.testesoftware.exerciciomock.exceptions.ParametroInvalidoException;
import sun.security.util.PendingException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francisco on 23/11/2016.
 */
public class CarrinhoCompras {

    private final IServicoConsultaCredito servicoConsultaCredito;
    private final IServicoConsultaEndereco servicoConsultaEndereco;
    private final Cliente cliente;

    protected CarrinhoCompras(
            IServicoConsultaCredito servicoConsultaCredito,
            IServicoConsultaEndereco servicoConsultaEndereco,
            Cliente cliente
    ) {
        this.servicoConsultaCredito = servicoConsultaCredito;
        this.servicoConsultaEndereco = servicoConsultaEndereco;
        this.cliente = cliente;
    }

    /**
     * Consulta os dados financeiros do cliente e empresa permite o financiamento para clientes que não possuem pendências
     *
     * @return se pode ou não vender financiado
     */
    public boolean podeVenderFinanciado() throws ParametroInvalidoException, NetworkError {
        return ! servicoConsultaCredito.estahNoSPC(cliente.getCPF());
    }

    List<ItemCompra> itemCompraList= new ArrayList<ItemCompra>();

    /**
     * Adiciona um item ao carrinho de compras
     * @param produto
     * @param qtd
     * @throws ParametroInvalidoException
     */
    public void adicionarItemCarrinho(Produto produto, int qtd) throws ParametroInvalidoException{
        itemCompraList.add(new ItemCompra(produto,qtd));
    }

    /**
     * remove um item do carrinho de compras
     * @param item
     * @throws ParametroInvalidoException
     */
    public void removerItemCarrinho(Produto item) throws ParametroInvalidoException{
        itemCompraList.remove(item);
    }

    /**
     * Retorna os itens incluídos no carrinho
     * @return
     */
    public List<Produto> getProdutos(){
        List<Produto> lista = new ArrayList<Produto>(itemCompraList.size());
        for(ItemCompra i : itemCompraList){
            lista.add(i.getProduto());
        }
        return lista;
    }

    /**
     * Obtem o valor total da venda incluindo o valor do frete
     * @param enderecoEntrega caso não seja especificado, utilizar o endereço do cliente.
     * @return
     */
    public double getValorTotal(Endereco enderecoEntrega){
        double valor = 0;
        for(ItemCompra i : itemCompraList){
            valor += i.getProduto().getValor() * i.getQuantidade();
        }
        return valor;
    }

    /**
     * Calcula o valor do frete
     * @return
     * @throws ParametroInvalidoException
     * @throws NetworkError
     */
    public double getValorFrete() throws ParametroInvalidoException, NetworkError {
        return servicoConsultaEndereco.obterValorFrete(cliente.getCPF(), 0);
    }

    public boolean validarCadastroCliente(){
        return cliente.isValid();
    }
    public List<IPendencia> getPendenciasDoCliente() throws ParametroInvalidoException, NetworkError {
        return servicoConsultaCredito.obterPendencias(cliente.getCPF());
    }

}

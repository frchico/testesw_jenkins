package br.edu.ifs.aulas.testesoftware.exerciciomock;

import br.edu.ifs.aulas.testesoftware.exerciciomock.exceptions.NetworkError;
import br.edu.ifs.aulas.testesoftware.exerciciomock.exceptions.ParametroInvalidoException;

/**
 * Created by Francisco on 23/11/2016.
 */
public interface IServicoConsultaEndereco {
    /**
     * Retorna o endereço dado um CEP
     * @param cep
     * @return
     * @throws ParametroInvalidoException
     * @throws NetworkError
     */
    Endereco getEnderecoPor(String cep)  throws ParametroInvalidoException, NetworkError;

    /**
     * Valida se todos os campos do endereço foram informados e se o cep só contém números
     * @param endereco
     * @return
     * @throws ParametroInvalidoException
     * @throws NetworkError
     */
    boolean validarDadosEnderecoNecessariosParaEntrega(Endereco endereco) throws ParametroInvalidoException, NetworkError;
    boolean ehCapital(String cep)  throws ParametroInvalidoException, NetworkError;
    /**
     * Calcula o valor do frete do cep informado.
     * Para capital o valor é de 5 reais por kg. Para interior o valor é de 15 reais por kg.
     * @param cep
     * @return o valor do frete do cep informado..
     */
    double obterValorFrete(String cep, double peso) throws ParametroInvalidoException, NetworkError;

}

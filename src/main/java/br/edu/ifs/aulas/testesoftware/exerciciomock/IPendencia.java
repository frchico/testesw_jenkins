package br.edu.ifs.aulas.testesoftware.exerciciomock;

import java.util.Date;

/**
 * Created by Francisco on 23/11/2016.
 */
public interface IPendencia {
    String getCPF();

    String getNome();

    String getNomeReclamante();

    String getDescricaoPendencia();

    Date getDataInclusao();

    double getValorPendencia();
}

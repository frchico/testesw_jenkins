package br.edu.ifs.aulas.testesoftware.exerciciomock;

/**
 * Created by Francisco on 23/11/2016.
 */
public interface Cliente {
    String getNome();
    String getCPF();

    /**
     * Verifica se os dados do cliente foram preenchidos corretamente
     * @return
     */
    boolean isValid();
    Endereco getEndereco();
    void setEndereco(Endereco endereco);
}

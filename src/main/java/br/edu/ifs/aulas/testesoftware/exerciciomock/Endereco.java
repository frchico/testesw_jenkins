package br.edu.ifs.aulas.testesoftware.exerciciomock;

/**
 * Created by Francisco on 23/11/2016.
 */
public abstract class Endereco {
    public abstract String getCep();
    public abstract String getLogradouro();
    public abstract String getBairro();
    public abstract String getCidade();
    public abstract String getEstado();

    /**
     * Retorna o endereço com 3 linhas seguindo a seguinte formatação:
     * 1 linha: Logradouro
     * 2 linha: Bairro - CEP
     * 3 linha: Cidade/Estado
     * @return Retorna o endereço com 3 linhas
     */
    public abstract String getEnderecoFormatadoParaEntrega();
}

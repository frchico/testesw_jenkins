package br.edu.ifs.aulas.testesoftware.exerciciomock;

/**
 * Created by Francisco on 23/11/2016.
 */
public class ItemCompra {
    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    Produto produto;
    int quantidade;

    public ItemCompra(Produto produto, int quantidade) {
        this.produto = produto;
        this.quantidade = quantidade;
    }
}
